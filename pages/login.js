import React, {useEffect, useState} from 'react'
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup'


import Link from 'next/link'
import { useRouter } from 'next/router'
import { signIn} from 'next-auth/react'



export default function Login() {

  const [username, setUsername] = useState()
  const [password, setPassword] = useState()

  const router = useRouter()
  const [authState, setAuthState] = useState({
    username: '',
    password: ''
  })
  const [pageState, setPageState] = useState({
    error: '',
    processing: false
  })
  const handleFielChange = (e) => {
    setAuthState(old => ({ ...old, [e.target.id]: e.target.value}))
  }

  const handleAuth = async () => {
    setPageState(old => ({...old, processing: true, error: ''}))
    signIn('credentials', {
      ...authState,
      redirect: false
    }).then(response => {
      console.log(response)
      if (response.ok) {
        router.push("/dashboard/")
      }else {
        setPageState(old => ({ ...old, processing: false, error: response.error}))
      }
    }).catch(error => {
      console.log(error)
      setPageState(old => ({...old, processing: false, error: error.message ?? "Something went wrong!"}))
    })
  }

 
  //////////////// VALIDATE USERS ////////////////

  // const sentData = async () => {
  //   var data = new FormData();
  //   data.append("username", "abc");
  //   data.append("password", "cdf");

  //   fetch("https://slogan.com.bo/vulcano/users/loginMobile", {
  //     method: 'POST',
  //     body: data,
  //   })
  //     .then(response => response.json())
  //     .then(data => {
  //       console.log('USER ID: ', data);
        
  //     },
  //       (error) => {
  //         console.log(error)
  //       }
  //     )

  // }


  ///////////////// VALIDATIONS INPUT ///////////////
  
    const validationSchema = Yup.object().shape({
        username: Yup.string()
        .required('Username is required'),
        password: Yup.string()
        .min(3, "Passaword must be at least 3 characters")
        .required("Password is required")
    })

    const formOptions = { resolver: yupResolver(validationSchema)};
    const {register, handleSubmit, reset, formState } = useForm(formOptions);
    const { errors } = formState;

    function onSubmit(data) {
        alert('SUCCESS!! :-)\n\n' + JSON.stringify(data, null, 4));
       // sentData()
        return false;
    }


  return (
     
    <div className="">

      <div className="grid grid-cols-12">

        <form onSubmit={handleSubmit(onSubmit)}>
          <div className="w-full h-full flex flex-col justify-center">
            <div className="col-span-12 md:col-span-6 lg:col-span-6 form-group ">

              <label htmlFor="first" className="text-[14px] mb-1 text-[#3A4567]">Usuario</label>

              <input id="username" name="username" type='text' className={`form-control ${errors.username ? 'is-invalid' : ''} w-[340px] h-[48px] rounded-[7px] bg-[#F6F6FA] border-[1px] border-[#E4E7EB] mb-3 px-3`}
                  {...register('username')} 
                  value={authState.username}
                  onChange={handleFielChange}
                  />

              <div className="invalid-feedback">{errors.username?.message}</div>
            </div>

            <div className="col-span-12 md:col-span-6 lg:col-span-6 form-group ">

              <label htmlFor="last" className="text-[14px] mb-1 text-[#3A4567]">Password</label>

              <input id="password" name="password" className={`form-control ${errors.password ? 'is-invalid' : ''} w-[340px] h-[48px] rounded-[7px] bg-[#F6F6FA] border-[1px] border-[#E4E7EB] mb-3 px-3`} 
                  {...register('password')}
                  value={authState.password}
                  onChange={handleFielChange} 
                  />

              <div className="invalid-feedback">{errors.password?.message}</div>
              
            </div>

            <button className="w-[340px] h-[48px] rounded-[7px] bg-[#3682F7] border-0 text-[#FFFFFF] text-[14PX]"
            //onClick={sentData}
            disabled={pageState.processing} 
            onClick={handleAuth}
            //type="submit"
            >
            
                    Ingresar
                
            </button>
          </div>
        </form>

      </div>

    </div>
  )
}


