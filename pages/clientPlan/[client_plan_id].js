import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router'
import Layout from '../components/layout';
import Image from 'next/image'

 /////// IMAGENES ////////////////////
 import ImgFacebook from '../../public/Dashhoard/facebook.svg'
 import ImgGmail from '../../public/Dashhoard/gmail.svg'
 import ImgIg from '../../public/Dashhoard/instagram.svg'
 import ImgMessenger from '../../public/Dashhoard/messenger.svg'
 import ImgTelegram from '../../public/Dashhoard/telegram.svg'
 import ImgTiktok from '../../public/Dashhoard/tikTok.svg'
 import ImgTwitter from '../../public/Dashhoard/twitter.svg'
 import ImgWhatsapp from '../../public/Dashhoard/whatsapp.svg'
 import ImgYoutube from '../../public/Dashhoard/youtube.svg'

import TablePost from "./Components/TablePost"
import TableAds from './Components/tableAds'
import ModalPost from "./Components/ModalPost"
import ModalAds from "./Components/ModalAds"
import CounterClient from './Components/counterClient';


export default function ClientPlan() {

    const [reloadPosts, setReloadPosts] = useState(false);
    const [reloadAds, setReloadAds] = useState(false);
    const [isLoading, setIsLoading] = useState(true)

    const [data, setData] = useState([ ]);

    const router = useRouter()
    const { client_plan_id } = router.query;
    
    useEffect(() => {

        if(router.isReady){

        }
        
    }, [router.isReady])
    


    return (
        <div className='ml-[30px] mt-[30px] mr-[30px]'>
            <div className='grid grid-cols-12'>
              <div className='col-span-12 md:col-span-12 lg:col-span-12 flex flex-row justify-between'>
                <div className='text-[24px] md:text-[32px] lg:text-[36px] text-[#000000] font-semibold'>
                    Client plan
                </div>
                <div>
                    <p className="text-[14px] md:text-[16px] lg:text-[18px] font-semibold text-right">Smart Start{client_plan_id}</p>
                    <p className="text-[10px] md:text-[16px] lg:text-[18px] font-medium text-right">Feb 2, 2022 Feb 8, 2022</p>

                </div>
                  
              </div>
              
            </div >

            <div className="mt-[20px] grid grid-cols-12 gap-3">
                <div className='col-span-6 md:col-span-6 lg:col-span-2 flex flex-row  bg-[#F3F3F3] rounded-[16px] justify-between p-[10px]'>
                    <div>
                        <text className="text-[24px] md:text-[28px] lg:text-[30px] font-semibold text-[#000000]"><CounterClient/></text>
                        <div className="text-[11px] text-[#000000]">PLAN TOTAL</div>
                    </div>
                </div>
                <div className='col-span-6 md:col-span-6 lg:col-span-2 flex flex-row  bg-[#F3F3F3] rounded-[16px] justify-between p-[10px]'>
                    <div>
                        <text className="text-[24px] md:text-[28px] lg:text-[30px]  font-semibold text-[#000000]"><CounterClient/></text>
                        <div className="text-[11px] text-[#000000]">ADS TOTAL</div>
                    </div>
                </div>
                <div className='col-span-6 md:col-span-6 lg:col-span-2 flex flex-row  bg-[#643DCE] rounded-[16px] justify-between p-[10px]'>
                    <div>
                        <text className="text-[24px] md:text-[28px] lg:text-[30px]  font-semibold text-[#FFFFFF]"><CounterClient/></text>
                        <div className="text-[11px] text-[#FFFFFF]">PLAN TOTAL</div>
                    </div>
                </div>
                <div className='col-span-6 md:col-span-6 lg:col-span-2 flex flex-row  bg-[#F3F3F3] rounded-[16px] justify-between p-[10px]'>
                    <div>
                        <text className="text-[24px] md:text-[28px] lg:text-[30px]  font-semibold text-[#000000]">Yes</text>
                        <div className="text-[11px] text-[#000000]">Feb 2,2022 11:20 P.M.</div>
                        <div className="text-[11px] text-[#000000]">PAID</div>
                    </div>
                </div>
                <div className='col-span-6 md:col-span-6 lg:col-span-2 flex flex-row  bg-[#F3F3F3] rounded-[16px] justify-between p-[10px]'>
                    <div>
                        <text className="text-[24px] md:text-[28px] lg:text-[30px] font-semibold text-[#000000]">Yes</text>
                        <div className="text-[11px] text-[#000000]">Feb 2,2022 11:20 P.M.</div>
                        <div className="text-[11px] text-[#000000]">INVOICED</div>
                    </div>
                </div>
                <div className='col-span-6 md:col-span-6 lg:col-span-2 flex flex-row  bg-[#643DCE] rounded-[16px] justify-between p-[10px]'>
                    <div>
                        <text className="text-[24px] md:text-[28px] lg:text-[30px] font-semibold text-[#FFFFFF]"><CounterClient/></text>
                        <div className="text-[11px] text-[#FFFFFF]">120.70 (Invoice)</div>
                        <div className="text-[11px] text-[#FFFFFF]">INVOICE TOTAL</div>
                    </div>
                </div>
            </div>

            <div className="mt-[24px] text-[18px] font-semibold">
            Status
            </div>
            <div className="mt-[20px] grid grid-cols-12 gap-3">
                <div className='col-span-6 md:col-span-6 lg:col-span-2 flex flex-row bg-gradient-to-l from-[#0062E0] to-[#19AFFF] rounded-[16px] justify-between p-[8px]'>
                    <div>
                    <div className="flex flex-row">
                        <text className="text-[28px] md:text-[30px] lg:text-[32px] font-semibold text-[#FFFFFF]">111/112</text>
                            
                    </div>
                    <div className="text-[10px] text-[#FFFFFF]">POST</div>
                    </div>
                    <div className='pt-[10px]'>
                    <Image
                        src={ImgFacebook}
                        layout='fixed'
                        alt='ImgFacebook'
                        width={36}
                        height={36}
                        
                    />
                    
                    </div>
                </div>
                <div className='col-span-6 md:col-span-6 lg:col-span-2 flex flex-row bg-gradient-to-l from-[#FCA759] via-[#E82D56] via-[#A22DB4] to-[#643DCE] rounded-[16px] justify-between pl-[10px] p-[8px]'>
                    <div>
                    <div className="flex flex-row">
                        <text className="text-[28px] md:text-[30px] lg:text-[32px] font-semibold text-[#FFFFFF]">111/112</text>
                            
                    </div>
                    <div className="text-[10px] text-[#FFFFFF]">POST</div>
                    </div>
                    <div className='pt-[10px]'>
                    <Image
                        src={ImgIg}
                        layout='fixed'
                        alt='ImgIg'
                        width={36}
                        height={36}
                        
                    />
                    
                    </div>
                </div>
                <div className='col-span-6 md:col-span-6 lg:col-span-2 flex flex-row  bg-[#161616] rounded-[16px] justify-between p-[8px]'>
                    <div>
                    <div className="flex flex-row">
                        <text className="text-[28px] md:text-[30px] lg:text-[32px] font-semibold text-[#FFFFFF]">111/112</text>
                            
                    </div>
                    <div className="text-[10px] text-[#FFFFFF]">POST</div>
                    </div>
                    <div className='pt-[10px]'>
                    <Image
                        src={ImgTiktok}
                        layout='fixed'
                        alt='ImgTiktok'
                        width={36}
                        height={36}
                        
                    />
                    
                    </div>
                </div>
                <div className='col-span-6 md:col-span-6 lg:col-span-2 flex flex-row bg-gradient-to-l from-[#0CA8F6] to-[#0096E1] rounded-[16px] justify-between p-[8px]'>
                    <div>
                    <div className="flex flex-row">
                        <text className="text-[28px] md:text-[30px] lg:text-[32px] font-semibold text-[#FFFFFF]">111/112</text>
                            
                    </div>
                    <div className="text-[10px] text-[#FFFFFF]">POST</div>
                    </div>
                    <div className='pt-[10px]'>
                    <Image
                        src={ImgTelegram}
                        layout='fixed'
                        alt='ImgTelegram'
                        width={36}
                        height={36}
                        
                    />
                    
                    </div>
                </div>
                <div className='col-span-6 md:col-span-6 lg:col-span-2 flex flex-row bg-gradient-to-l from-[#DB0505] to-[#FF0000] rounded-[16px] justify-between p-[8px]'>
                    <div>
                    <div className="flex flex-row">
                        <text className="text-[28px] md:text-[30px] lg:text-[32px] font-semibold text-[#FFFFFF]">111/112</text>
                            
                    </div>
                    <div className="text-[10px] text-[#FFFFFF]">POST</div>
                    </div>
                    <div className='pt-[10px]'>
                    <Image
                        src={ImgYoutube}
                        layout='fixed'
                        alt='ImgYoutube'
                        width={36}
                        height={36}
                        
                    />
                    
                    </div>
                </div>
                <div className='col-span-6 md:col-span-6 lg:col-span-2 flex flex-row bg-gradient-to-l from-[#FBBC04] to-[#FCD462] rounded-[16px] justify-between p-[8px]'>
                    <div>
                    <div className="flex flex-row">
                        <text className="text-[28px] md:text-[30px] lg:text-[32px] font-semibold text-[#FFFFFF]">111/112</text>
                            
                    </div>
                    <div className="text-[10px] text-[#FFFFFF]">POST</div>
                    </div>
                    <div className='pt-[10px]'>
                    <Image
                        src={ImgGmail}
                        layout='fixed'
                        alt='ImgGmail'
                        width={36}
                        height={36}
                        
                    />
                    
                    </div>
                </div>
            </div>

            <div className="mt-[24px] text-[18px] font-semibold">
            Ads overview
            </div>
            <div className="mt-[20px] grid grid-cols-12 gap-3">
                <div className='col-span-6 md:col-span-6 lg:col-span-3 flex flex-row'>
                    <Image
                        src={ImgFacebook}
                        layout='fixed'
                        alt='ImgFacebook'
                        width={56}
                        height={56}
                        
                    />
                    <div className="ml-[10px]">
                        <text className="text-[15px] md:text-[18px] lg:text-[18px] font-semibold text-[#000000]"><CounterClient/></text>
                        <div className="text-[13px] text-[#A7A7B7]">TOTAL ADS</div>
                    </div>
                    
                </div>
                <div className='col-span-6 md:col-span-6 lg:col-span-3 flex flex-row'>
                    <Image
                        src={ImgTelegram}
                        layout='fixed'
                        alt='ImgTelegram'
                        width={56}
                        height={56}
                        
                    />
                    <div className="ml-[10px]">
                        <text className="text-[15px] md:text-[18px] lg:text-[18px] font-semibold text-[#000000]"><CounterClient/></text>
                        <div className="text-[13px] text-[#A7A7B7]">TOTAL ADS</div>
                    </div>
                    
                </div>
                <div className='col-span-6 md:col-span-6 lg:col-span-3 flex flex-row'>
                    <Image
                        src={ImgIg}
                        layout='fixed'
                        alt='ImgIg'
                        width={56}
                        height={56}
                        
                    />
                    <div className="ml-[10px]">
                        <text className="text-[15px] md:text-[18px] lg:text-[18px] font-semibold text-[#000000]"><CounterClient/></text>
                        <div className="text-[13px] text-[#A7A7B7]">TOTAL ADS</div>
                    </div>
                    
                </div>
                <div className='col-span-6 md:col-span-6 lg:col-span-3 flex flex-row'>
                    <Image
                        src={ImgYoutube}
                        layout='fixed'
                        alt='ImgYoutube'
                        width={56}
                        height={56}
                        
                    />
                    <div className="ml-[10px]">
                        <text className="text-[15px] md:text-[18px] lg:text-[18px] font-semibold text-[#000000]"><CounterClient/></text>
                        <div className="text-[13px] text-[#A7A7B7]">TOTAL ADS</div>
                    </div>
                </div>

            </div>

            <div className='grid grid-cols-12 gap-3 mt-[35px]'>
              <div className='col-span-12 md:col-span-12 lg:col-span-5'>
                <div className='grid grid-cols-12'>
                    <div className='col-span-12 md:col-span-12 lg:col-span-12 flex flex-row justify-between'>
                        <div className='text-[17px] md:text-[17px] lg:text-[17px] text-[#000000] font-semibold'>Posts</div>
                        <div className='flex flex-row'>

                            {router.isReady &&
                            <ModalPost client_plan_id={client_plan_id} reloadPosts={reloadPosts} setReloadPosts={setReloadPosts} />
                            }
                        </div>
                        
                    </div>
                
                </div >
                <div  className='grid grid-cols-12'>
                    <div className='col-span-12 md:col-span-12 lg:col-span-12'>
                    {router.isReady &&
                        <TablePost client_plan_id={client_plan_id} reloadPosts={reloadPosts} />
                    }
                    </div>

                </div>
                  
              </div>

              <div className='col-span-12 md:col-span-12 lg:col-span-7'>
                <div className='grid grid-cols-12'>
                    <div className='col-span-12 md:col-span-12 lg:col-span-12 flex flex-row justify-between'>
                        <div className='text-[17px] md:text-[17px] lg:text-[17px] text-[#000000] font-semibold'>Ads</div>
                        <div className='flex flex-row '>
                        {router.isReady &&
                            <ModalAds client_plan_id={client_plan_id}  reloadAds={reloadAds} setReloadAds={setReloadAds}  />
                        }
                        </div>
                        
                    </div>

                    <div className='col-span-12 md:col-span-12 lg:col-span-12'>
                    {router.isReady &&
                        <TableAds client_plan_id={client_plan_id} reloadAds={reloadAds} />
                    }
                    </div>
                
                </div >
                  
              </div>

              
            </div>
            {/* </div>
            )} */}
     </div>
    )
}
ClientPlan.getLayout = function getLayout(page){
    return (
      <Layout>{page}</Layout>
    )
  }