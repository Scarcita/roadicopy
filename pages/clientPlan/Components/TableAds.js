import React from 'react';
import Spinner from 'react-activity/dist/Spinner';
import { useRouter } from 'next/router'
import 'react-activity/dist/Spinner.css';
import Image from 'next/image';

import ImgFacebook from "../../../public/SocialMedia/Facebook.svg";
import logo from "../../../public/ClientPlan/logo.svg"
import marca from "../../../public/ClientPlan/marca.svg"
import editar from "../../../public/ClientPlan/editar.svg"



import { useEffect, useState } from 'react'

const TableAds = (props) => {

    const {reloadAds, client_plan_id} = props
    
    const [isLoading, setIsLoading] = useState(true);
    const [total, setTotal] = useState(0);
    const [data, setData] = useState([]);

    useEffect(() => {
        fetch('https://slogan.com.bo/roadie/clientsPlansPostsAds/all/' + client_plan_id)
            .then(response => response.json())
            .then(data => {
                if (data.status) {
                    setData(data.data)
                } else {
                    console.error(data.error)
                }
                setIsLoading(false)
            })

        console.warn('reloaddd' + reloadAds);

    }, [reloadAds])

    return (
        isLoading ?
            <>
                <div className='flex justify-center items-center' style={{ width: '100%', height: 70 }}>
                    <Spinner color="#582BE7" size={17} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
                </div>

            </>
            :
            <>
                <div className='rounded-[22.35px] pt-6 h-full w-full overflow-auto'>
                    <TablaProductos data={data} client_plan_id={client_plan_id } />
                </div>
            </>
    )
}


const TablaProductos = (props) => {
    const { data, client_plan_id } = props;

    const splitData = (obj) => {
        data.forEach(element => {
            let date = element.created.split("T");
            element.created = date[0];
        });
        data = obj;
    };
    splitData(data);

    return (

        <div className="  bg-[#FFFF] grid grid-cols-12 ">
            <table className="col-span-12 md:col-span-12 lg:col-span-12">
                <thead>
                    <tr className='text-[12px] font-semibold text-[#000000] border-b-2 border-[#D9D9D9] p-[5px]'>
                        <th>Date</th>
                        <th>SN</th>
                        <th>Post</th>
                        <th>Status</th>
                        <th>Amount</th>
                    </tr>
                </thead>

                <tbody>

                    {data.map(row =>
                     {

                        const imgUrl = "";

                        if(row._matchingData.ClientsPlansPosts.social_network !== null){
                            switch (row._matchingData.ClientsPlansPosts.social_network) {
                                case 'Facebook': 
                                    imgUrl = '/SocialMedia/Facebook.svg'
                                    break;
                                case 'TikTok':
                                    imgUrl= '/SocialMedia/TikTok.svg'
                                    break;
                        
                                case 'Instagram':
                                    imgUrl = '/SocialMedia/Instagram.svg'
                                break;
                                case 'YouTube':
                                    imgUrl = '/SocialMedia/Youtube.svg'
                                break;
                                case 'Mailing':
                                    imgUrl = '/Plans/gmail.svg'
                                break;
                                case 'LinkedIn':
                                    imgUrl = '/SocialMedia/messenger.svg'
                                break;
                                case 'Twitter':
                                    imgUrl = '/SocialMedia/Twitter.svg'
                                break;
                                    
                                default:
                                    break;
                            }
                        }

                        return (
                            <tr key={row.id} >
                            <td className='h-8 w-16 text-center text-[10px]'>
                                {row.created}
                            </td>
                           
                            <td className='h-8 w-16 text-center '>
                                    {row._matchingData.ClientsPlansPosts.social_network !== null ?
                                        <Image
                                            src={imgUrl}
                                            alt='ImgFacebook'
                                            layout='fixed'
                                            width={30}
                                            height={30}
                                        />
                                        : <></>
                                    }

                            </td>
                            <td className='h-8 w-16 text-center '>

                                <div className='flex flex-row text-center'>
                                    {row._matchingData.ClientsPlansPosts.media_url !== null && row._matchingData.ClientsPlansPosts.media_url.trim().length !== 0 ?
                                        <Image
                                            className='rounded-md bg-[#D9D9D9] p-2 '
                                            src={row._matchingData.ClientsPlansPosts.media_url}
                                            alt=''
                                            layout='fixed'
                                            width={30}
                                            height={30}
                                        />
                                        : <></>
                                    }
                                    <div className='text-[12px] ml-[10px]'>
                                        {row._matchingData.ClientsPlansPosts.title}
                                        <div>
                                            <small className='text-gray-500 text-xs'>{row._matchingData.ClientsPlansPosts.subtitle}</small>
                                        </div>
                                        {row._matchingData.ClientsPlansPosts.post_copy}
                                    </div>
                                </div>


                            </td>


                            <td className='h-8 w-16 text-center text-[10px]'>
                                {row._matchingData.ClientsPlansPosts.status}

                            </td>
                            <td className='h-8 w-16 text-center text-[10px] font-semibold text-[#643DCE]'>
                                {row.amount !== null ?

                                        <div>
                                            {row.amount} USD
                                        </div>
                                        : <></>

                                }
                            </td>

                            <td className='h-8 w-16 text-center text-[10px] font-semibold text-[#643DCE] p-[5px]'>
                                <Image
                                src={editar}
                                layout="fixed"
                                alt='editar'
                                width={25}
                                height={25}
                                />
                              
                            </td>
                            

                        </tr>

                        )
                    }

                        
                    )}


                </tbody>
            </table>
        </div>
    );
};





export default TableAds;