import React from 'react';
import Spinner from 'react-activity/dist/Spinner';
import 'react-activity/dist/Spinner.css';
import Image from 'next/image';
import { useRouter } from 'next/router'

 import imagenFaceboock from "../../../public/SocialMedia/Facebook.svg";
import logoUrl from "../../../public/SocialMedia/Instagram.svg";
import ImagenYoutube from "../../../public/SocialMedia/Youtube.svg"
import ImagenTikTok from "../../../public/SocialMedia/TikTok.svg"
import ImgTwitter from "../../../public/SocialMedia/Twitter.svg"
import ImgLinkedin from "../../../public/SocialMedia/messenger.svg"
import logo from "../../../public/ClientPlan/logo.svg"
import marca from "../../../public/ClientPlan/marca.svg"
import editar from "../../../public/ClientPlan/editar.svg"



import { useEffect, useState } from 'react'

const TablePost = (props) => {

    const {reloadPosts, client_plan_id} = props;

    const [isLoading, setIsLoading] = useState(true);
    const [data, setData] = useState([]);

    useEffect(() => {
        fetch('https://slogan.com.bo/roadie/clientsPlansPosts/all/' + client_plan_id)
            .then(response => response.json())
            .then(data => {
                if (data.status) {
                    setData(data.data)
                } else {
                    console.error(data.error)
                }
                setIsLoading(false)
            })

            console.warn('reloaddd' + reloadPosts);

    }, [reloadPosts])

    return (
        isLoading ?
            <>
                <div className='flex justify-center items-center' style={{ width: '100%', height: 70 }}>
                    <Spinner color="#582BE7" size={17} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
                </div>

            </>
            :
            <>
                <div className='rounded-[22.35px] pt-6 h-full w-full overflow-auto'>
                    <TablaProductos data={data} client_plan_id={client_plan_id }/>
                </div>
            </>
    )
}


const TablaProductos = (props) => {
    const { data, client_plan_id } = props;

    const splitData = (obj) => {
        data.forEach(element => {
            let date = element.created.split("T");
            element.created = date[0];
        });
        data = obj;
    };
    splitData(data);

    return (

        <div className=" grid grid-cols-12 ">
            <table className="col-span-12 md:col-span-12 lg:col-span-12">
                <thead>
                    <tr className='text-[12px] font-semibold text-[#000000] border-b-2 border-[#D9D9D9] text-center'>
                        <th>Date</th>
                        <th>SN</th>
                        <th>Post</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                </thead>

                <tbody>

                    {data.map(row =>
                        {

                            const imgUrl = "";

                            if(row.social_network !== null){
                                switch (row.social_network) {
                                    case 'Facebook': 
                                        imgUrl = '/SocialMedia/Facebook.svg'
                                        break;
                                    case 'TikTok':
                                        imgUrl= '/SocialMedia/TikTok.svg'
                                        break;
                                    case 'Instagram':
                                        imgUrl = '/SocialMedia/Instagram.svg'
                                    break;
                                    case 'YouTube':
                                        imgUrl = '/SocialMedia/Youtube.svg'
                                    break;
                                    case 'Mailing':
                                        imgUrl = '/Plans/gmail.svg'
                                    break;
                                    case 'LinkedIn':
                                        imgUrl = '/SocialMedia/messenger.svg'
                                    break;
                                    case 'Twitter':
                                        imgUrl = '/SocialMedia/Twitter.svg'
                                    break;
                                        
                                    default:
                                        break;
                                }
                            }
                            
                        return (
                        <tr key={row.id} >
                            <td className='h-8 w-16 text-[10px]'>
                                {row.created}
                            </td>
                            <td className='h-8 w-16 text-[12px] text-center '>
                                {row.social_network !== null ?
                                    <Image
                                        src={imgUrl}
                                        alt=''
                                        layout='fixed'
                                        width={30}
                                        height={30}
                                    />
                                    : <></>
                                }

                            </td>

                            <td className='h-8 w-16 text-center'>


                                <div className='flex flex-row text-center'>
                                    {row.media_url !== null && row.media_url.trim().length !== 0 ?
                                        <Image
                                            className='rounded-md bg-[#D9D9D9] p-2 '
                                            src={row.media_url}
                                            alt=''
                                            layout='fixed'
                                            width={30}
                                            height={30}
                                        />
                                        : <></>
                                    }
                                    <div className='text-[12px] ml-[10px]'>

                                        {row.post_copy}

                                    </div>
 
                                </div>


                            </td>


                            <td className='h-8 w-16 text-center items-center'>
                                    <div className='w-[52px] h-[17px] bg-[#D9D9D9] rounded-[6px] text-[8px] text-center items-center pt-[2px]'>
                                    {row.status}
                                    
                                    </div>
                            </td>
                            <td className='h-8 w-16 text-center items-center'>
      
                                <Image
                                src={editar}
                                layout="fixed"
                                alt='editar'
                                width={25}
                                height={25}
                                />

                            </td>
                           


                        </tr>
                            )


                        }
                    )}


                </tbody>
            </table>
        </div>
    );
};





export default TablePost;