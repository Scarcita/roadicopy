import React from 'react';
import { useEffect, useState } from 'react'
import Spinner from 'react-activity/dist/Spinner';
import 'react-activity/dist/Spinner.css';
import Image from 'next/image';
import { useRouter } from 'next/router';
import Link from 'next/link';

import imagenFaceboock from "../../../public/SocialMedia/Facebook.svg";
import logo from "../../../public/ClientPlan/logo.svg"
import marca from "../../../public/ClientPlan/marca.svg"
import editar from "../../../public/ClientPlan/editar.svg"





const TableDashoardCli = (props) => {
    

    const {reloadClients, client_id} = props
    //const {reloadPosts, reloadAds, client_id} = props

    const [isLoading, setIsLoading] = useState(true)
    const [data, setData] = useState([]);
    const router = useRouter()

    useEffect(() => {
        fetch('https://slogan.com.bo/roadie/clientsPlans/all/' + client_id)
            .then(response => response.json())
            .then(data => {
                if (data.status) {
                    console.log(data.data);
                    setData(data.data)

                } else {
                    console.error(data.error)
                }
                setIsLoading(false)
            })

            console.warn('reloaddd' + reloadClients);

        }, [reloadClients])

    return (
        isLoading ?
            <>
                <div className='flex justify-center items-center' style={{ width: '100%', height: 70 }}>
                    <Spinner color="#582BE7" size={17} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
                </div>

            </>
            :
            <>
                <div className='rounded-[22.35px] pt-6 h-full w-full overflow-auto'>
                    <TablaProductos data={data} client_id={client_id} />
                </div>
            </>
    )
}


const TablaProductos = (props) => {
    const { data, client_id } = props;
    console.log("assssssssssssssssssss");
    console.log(data);
    console.log(props);
    const map1 = data.map(row => console.log(row));

    const splitData = (obj) => {
        data.forEach(element => {
            let date = element.created.split("T");
            element.created = date[0];
        });
        data = obj;
    };
    splitData(data);

    return (

        <div className=" bg-[#FFFF] grid grid-cols-12 ">
            <table className="col-span-12 md:col-span-12 lg:col-span-12">
                <thead>
                    <tr className='text-[14px] md:text-[17px] lg:text-[17px] font-semibold text-[#000000] border-b-2 border-[#D9D9D9]'>
                        <th>Plan</th>
                        <th>Dates</th>
                        <th>Service Total</th>
                        <th>Ads Total</th>
                        <th>Total</th>
                        <th>Paid</th>
                        <th>Invoice</th>
                        <th>Total w/Invoice</th>
                        <th></th>
                    </tr>
                </thead>

                <tbody>

                    {data.map(row =>

                        <tr key={row.id} >
                            <td className='h-8 w-16 text-center text-[14px] font-medium] p-[5px]'>
                                {row.plan.name}
                            </td>
                           
                            <td className='h-8 w-16 text-center text-[13px] p-[5px]'>
                                <div>

                                    {row.start_date}

                                </div>
                                
                                <div>

                                    {row.end_date}

                                </div>

                            </td>
                            <td className='h-8 w-16 text-center text-[24px] font-semibold text-cente p-[5px]'>

                                    {row.service_total}
   
                            </td>


                            <td className='h-8 w-16 text-[24px] font-semibold text-center p-[5px]'>
                                {row.ads_total}

                            </td>
                            <td className='h-8 w-16  text-[24px] font-semibold text-center p-[5px]'>
                                {row.total}

                            </td>
                            <td className='h-8 w-16 text-center p-[5px]'> 
                                <div className='text-[#643DCE] font-semibold text-[14px] '>
                                    {row.paid}
                                </div>
                                <div className='text-[#643DCE] text-[10px]'>
                                    {row.paid_date}
                                </div>

                            </td>

                            <td className='h-8 w-16 text-center p-[5px]'> 
                                <div className='text-[#643DCE] font-semibold text-[14px]'>
                                    {row.invoiced}
                                </div>
                                <div className='text-[#643DCE] text-[10px]'>
                                    {row.invoiced_date}
                                </div>

                            </td>

                            <td className='h-8 w-16 p-[5px]'> 
                                <div className='text-[24px] font-semibold text-center'>
                                {row.total_with_invoice}
                                </div>
                                
                                {row.invoice_number !== null ?
                                    <div className='text-[12px] text-center'>
                                    {row.invoice_number} (Invoice)
                                    </div>
                                : <></>
                                }

                            </td>
                            <td className='h-8 w-5  text-end p-[5px]'> 
                                <div className='w-10 h-10'>
                                <Link
                                    href={`/clientPlan/${encodeURIComponent(row.id)}`}
                                >
                                     <Image
                                        src={editar}
                                        layout="fixed"
                                        alt='editar'
                                        width={30}
                                        height={30}
                                         />
                                </Link>
                                </div>

                            </td>


                        </tr>
                    )}


                </tbody>
            </table>
        </div>
    );
};





export default TableDashoardCli;