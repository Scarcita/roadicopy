import React from 'react';


import Spinner from 'react-activity/dist/Spinner';
import 'react-activity/dist/Spinner.css';
import Image from 'next/image';

import ImagenFacebook from "../../../public/SocialMedia/Facebook.svg";
import logo from "../../../public/logo.svg"
import marca from "../../../public/marca.svg"
import editar from "../../../public/editar.svg"



import { useEffect, useState } from 'react'

const Table = (props) => {

    const {reloadAd} = props

    const [isLoading, setIsLoading] = useState(true)

    const [showDots, setShowDots] = useState(true);
    const [total, setTotal] = useState(0);
    const [data, setData] = useState([]);

    useEffect(() => {
        fetch('https://slogan.com.bo/roadie/clientsPlansPostsAds/all/1')
            .then(response => response.json())
            .then(data => {
                if (data.status) {
                    console.log(data.data);
                    setData(data.data)

                } else {
                    console.error(data.error)
                }
                setIsLoading(false)
            })

            console.warn('reloaddd' + reloadAd);

    }, [reloadAd])

    return (
        isLoading ?
            <>
                <div className='flex justify-center items-center' style={{ width: '100%', height: 70 }}>
                    <Spinner color="#582BE7" size={17} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
                </div>

            </>
            :
            <>
                <div className=' rounded-[22.35px] pt-6 h-full w-full overflow-auto'>
                    <TablaProductos data={data} />
                </div>
            </>
    )
}


const TablaProductos = (props) => {
    const { data } = props;
    console.log("assssssssssssssssssss");
    console.log(data);
    console.log(props);
    const map1 = data.map(row => console.log(row));

    const splitData = (obj) => {
        data.forEach(element => {
            let date = element.created.split("T");
            element.created = date[0];
        });
        data = obj;
    };
    splitData(data);

    return (

        <div className="grid grid-cols-12">
            <table className="col-span-12 md:col-span-12 lg:col-span-12">
                <thead>
                    <tr className='text-[14px] md:text-[17px] lg:text-[17px] font-semibold text-[#000000] border-b-2 border-[#D9D9D9]'>
                        <th>Date</th>
                        <th>Client</th>
                        <th>Social Network</th>
                        <th>Post</th>
                        <th> Duaration</th>
                        <th>Amount</th>
                    </tr>
                </thead>

                <tbody>

                    {data.map(row =>
                     {

                        const imgUrl = "";

                        if(row._matchingData.ClientsPlansPosts.social_network !== null){
                            switch (row._matchingData.ClientsPlansPosts.social_network) {
                                case 'Facebook': 
                                    imgUrl = '/SocialMedia/Facebook.svg'
                                    break;
                                case 'TikTok':
                                    imgUrl= '/SocialMedia/TikTok.svg'
                                    break;
                        
                                case 'Instagram':
                                    imgUrl = '/SocialMedia/Instagram.svg'
                                break;
                                case 'YouTube':
                                    imgUrl = '/SocialMedia/Youtube.svg'
                                break;
                                case 'Mailing':
                                    imgUrl = '/Plans/gmail.svg'
                                break;
                                case 'LinkedIn':
                                    imgUrl = '/SocialMedia/messenger.svg'
                                break;
                                case 'Twitter':
                                    imgUrl = '/SocialMedia/Twitter.svg'
                                break;
                                    
                                default:
                                    break;
                            }
                        }
                        return (

                            <tr key={row.id} >
                            <td className='h-8 w-16 text-[14px] text-center '>
                                {row.created}
                            </td>
                            <td className='h-8 w-16 font-bold'>
                                <div className='flex flex-row'>
                                    {row._matchingData.ClientsPlansPosts.media_url !== null ?
                                        <Image
                                            className='rounded-full bg-[#D9D9D9] p-2 '
                                            src={row._matchingData.ClientsPlansPosts.media_url}
                                            alt=''
                                            layout='fixed'
                                            width={30}
                                            height={30}
                                        />
                                        : <></>
                                    }
                                    <div className='ml-[10px] text-[16px]'>
                                        {row._matchingData.ClientsPlansPosts.client_plan_id}
                                    </div>

                                </div>


                            </td>
                            <td className='h-8 w-16 text-center text-[14px] font-semibold'>
                                <div  className=''>
                                        {row._matchingData.ClientsPlansPosts.social_network !== null ?
                                            <Image
                                                src={imgUrl}
                                                alt='imagenFaceboock'
                                                layout='fixed'
                                                width={30}
                                                height={30}
                                            />
                                            : <></>
                                        }
                                    </div>

                            </td>
                            <td className='h-8 w-16 text-[12px] text-center'>

                                <div className='flex flex-row'>
                                    <div className='mr-[5px]'>
                                        {row._matchingData.ClientsPlansPosts.media_url !== null ?
                                            <Image
                                                className='rounded-lg bg-[#D9D9D9]'
                                                src={row._matchingData.ClientsPlansPosts.media_url}
                                                alt=''
                                                layout='fixed'
                                                width={30}
                                                height={30}
                                            ></Image>
                                            : <></>
                                        }
                                    </div>

                                    {row._matchingData.ClientsPlansPosts.post_copy}
                                </div>


                            </td>


                            <td className='h-8 w-16 text-[12px] text-center'>
                                <div>
                                    {row.start_datetime}

                                </div>
                                
                                <div>
                                    {row.end_datetime}
                                </div>

                            </td>
                            <td className='h-8 w-16 text-[#643DCE] text-[20px]  font-semibold'>

                            {row.amount !== null ?
                                <div>
                                {row.amount}
                                <span className='text-[12px]'> USD</span>
                                </div>
                                : <></>
                            }

                            </td>
                            <td className='h-8 w-16 text-center'>
                                <div className='w-10 h-10'>
                                    <Image
                                        src={editar}
                                        layout="fixed"
                                        alt='editar'
                                        width={30}
                                        height={30}
                                        />
                                </div>

                            </td>


                        </tr>

                        )
                    }

                    )}


                </tbody>
            </table>

            {/* <div className="p-5 bg-gray-100">
                <div className="grid grid-cols-1 lg:grid-cols-2 gap-4 md:block  lg:hidden">
                    <div className="bg-[#F6F6F6] space-y-3 p-4 rounded-lg overflow-auto">
                        {data.map(row =>

                            <div className="grid grid-cols-3 items-center text-sm rounded bg-[#fff] shadow py-4">

                                <div className='w-2/5 m-auto'>
                                    <div className='mb-2'>
                                        <div className='text-4xl font-bold text-[#3682F7]'>
                                            Client
                                        </div>

                                        <div className='flex flex-row'>
                                            <div className='mr-2'>
                                                <Image
                                                    src={logo}
                                                    alt='logo'
                                                    layout='fixed'
                                                />
                                            </div>

                                            {row.contact_name}
                                        </div>

                                    </div>

                                    <div>

                                        <div className='text-[#643DCE]'>
                                            <Image
                                                src={ImagenFacebook}
                                                alt="brand"
                                                layout="fixed"
                                                width={40}
                                                height={40}
                                            />

                                        </div>
                                    </div>


                                </div>


                                <div className='w-2/5 m-auto'>
                                    <div className='flex flex-col'>
                                        <div className='text-4xl font-bold text-[#3682F7] '>
                                            Post
                                        </div>
                                        <div className='flex flex-row'>

                                            <div className='w-1/3'>
                                                <Image
                                                    src={marca}
                                                    alt="brand"
                                                    layout="fixed"
                                                    width={80}
                                                    height={80}
                                                />
                                            </div>

                                            <div className='w-2/3'>
                                                <div className='mt-2 ml-2'>
                                                    {row.car.cars_models_version.cars_model.name}
                                                </div>
                                                <div className=" mb-2 text-black font-bold flex flex-row">
                                                    {row.created}
                                                </div>

                                                <div className=" mb-2 text-black font-bold flex flex-row">
                                                    {row.created}
                                                </div>

                                                <div className=" mb-2 text-black font-bold flex flex-row">
                                                    {row.car.plate}
                                                </div>
                                            </div>

                                        </div>

                                    </div>




                                </div>


                                <div className='w-1/5 flex justify-center items-center m-auto ' >
                                    <Image
                                        src={editar}
                                        layout="fixed"
                                        alt='editar'
                                        width={30}
                                        height={30}
                                    />
                                </div>




                            </div>
                        )}


                    </div>

                </div>
            </div> */}

        </div>
    );
};





export default Table;