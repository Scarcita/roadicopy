import React from 'react';
import Spinner from 'react-activity/dist/Spinner';
import 'react-activity/dist/Spinner.css';
import Image from 'next/image';





///////////// IMAGES //////////////

import ImgFacebook from '../../../public/Plans/facebook.svg'
import ImgIg from '../../../public/Plans/instagram.svg'
import ImgTiktok from '../../../public/Plans/tikTok.svg'
import ImgYoutube from '../../../public/Plans/youtube.svg'
import ImgTelegram from '../../../public/Plans/telegram.svg'
import ImgTwitter from '../../../public/Plans/twitter.svg'
import editar from "../../../public/editar.svg"



import { useEffect, useState } from 'react'

const TablePlans = (props) => {

    const {reloadPlans} = props
    const [isLoading, setIsLoading] = useState(true)

    const [showDots, setShowDots] = useState(true);
    const [total, setTotal] = useState(0);
    const [data, setData] = useState([]);

    useEffect(() => {
        fetch('https://slogan.com.bo/roadie/plans/all/1')
            .then(response => response.json())
            .then(data => {
                if (data.status) {
                    console.log(data.data);
                    setData(data.data)

                } else {
                    console.error(data.error)
                }
                setIsLoading(false)
            })

            console.warn('reloaddd' + reloadPlans);

    }, [reloadPlans])

    return (
        isLoading ?
            <>
                <div className='flex justify-center items-center' style={{ width: '100%', height: 70 }}>
                    <Spinner color="#582BE7" size={17} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
                </div>

            </>
            :
            <>
                <div className=' rounded-[22.35px] pt-6 h-full w-full overflow-auto'>
                    <TablaProductos data={data} />
                </div>
            </>
    )
}


const TablaProductos = (props) => {
    const { data } = props;
    console.log("assssssssssssssssssss");
    console.log(data);
    console.log(props);
    const map1 = data.map(row => console.log(row));

    const splitData = (obj) => {
        data.forEach(element => {
            let date = element.created.split("T");
            element.created = date[0];
        });
        data = obj;
    };
    splitData(data);

    return (

        <div className="grid grid-cols-12">
            <table className="col-span-12 md:col-span-12 lg:col-span-12">
                <thead>
                    <tr className='text-[14px] md:text-[17px] lg:text-[17px] font-semibold text-[#000000] border-b-2 border-[#D9D9D9]'>
                        <th>Name</th>
                        <th>Post</th>
                        <th>Has included ads</th>
                        <th>Price</th>
                        <th></th>
                    </tr>
                </thead>

                <tbody>

                    {data.map(row =>

                        <tr key={row.id} >
                            <td className='h-8 w-16'>
                                {row.name}
                            </td>
                            <td className='h-8 w-16 font-semibold '>
                                <div className='flex flex-row'>
                                    
                                    <div  className='ml-[10px] text-[12px]'>{row.facebook_posts_qty}

                                        {row.facebook_posts_qty !== null ?
                                            <Image
                                                src={ImgFacebook}
                                                alt='ImgIg'
                                                layout='fixed'
                                                width={30}
                                                height={30}
                                            />
                                            : <></>
                                        }
                                    </div>
                                    
                                    <div  className='ml-[10px] text-[12px]'>{row.instagram_posts_qty}

                                        {row.instagram_posts_qty !== null ?
                                            <Image
                                                src={ImgIg}
                                                alt='ImgIg'
                                                layout='fixed'
                                                width={30}
                                                height={30}
                                            />
                                            : <></>
                                        }
                                    </div>
                                    <div  className='ml-[10px] text-[12px]'>{row.mailing_posts_qty}

                                        {row.mailing_posts_qty !== null ?
                                            <Image
                                                src={ImgTelegram}
                                                alt='ImgIg'
                                                layout='fixed'
                                                width={30}
                                                height={30}
                                            />
                                            : <></>
                                        }
                                    </div>
                                    <div  className='ml-[10px] text-[12px]'>{row.youtube_posts_qty}
                                        {row.youtube_posts_qty !== null ?
                                            <Image
                                                src={ImgYoutube}
                                                alt='ImgYoutube'
                                                layout='fixed'
                                                width={30}
                                                height={30}
                                            />
                                            : <></>
                                        }
                                    </div>
                                    <div  className='text-[12px]'>{row.tiktok_posts_qty}
                                        {row.tiktok_posts_qty !== null ?
                                            <Image
                                                src={ImgTiktok}
                                                alt='ImgYoutube'
                                                layout='fixed'
                                                width={30}
                                                height={30}
                                            />
                                            : <></>
                                        }
                                    </div>
                                    <div  className='ml-[10px] text-[12px]'>{row.linkedin_posts_qty}
                                        {row.linkedin_posts_qty !== null ?
                                            <Image
                                                src={ImgTelegram}
                                                alt='ImgYoutube'
                                                layout='fixed'
                                                width={30}
                                                height={30}
                                            />
                                            : <></>
                                        }
                                    </div>
                                    <div  className='ml-[10px] text-[12px]'>{row.twitter_posts_qty}
                                        {row.twitter_posts_qty !== null ?
                                            <Image
                                                src={ImgTwitter}
                                                alt='ImgYoutube'
                                                layout='fixed'
                                                width={30}
                                                height={30}
                                            />
                                            : <></>
                                        }
                                    </div>
                                </div>


                            </td>
                            
                            <td className='h-8 w-16 text-center]'>
                                <div className='text-[20px] font-semibold text-[#582BE7] text-center'>
                                {row.has_included_ads}
                                </div>

                                {row.included_ads_qty !== null ?
                                <div className='text-[12px] text-[#000000] text-center'>
                                    {row.included_ads_qty} USD

                                </div>
                                : <></>
                                }

                            </td>
                            
                            <td className='h-8 w-16 text-center'>
                            {row.price !== null ?
                                <div>
                                    {row.price} USD
                                </div>
                                : <></>
                            }

                            </td>

                            <td className='h-8 w-16 text-center'>
                                    <Image
                                        src={editar}
                                        layout="fixed"
                                        alt='editar'
                                        width={30}
                                        height={30}
                                        />

                            </td>

                        </tr>
                    )}


                </tbody>
            </table>

        </div>
    );
};





export default TablePlans;