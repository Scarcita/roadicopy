import React from 'react';


import Spinner from 'react-activity/dist/Spinner'
import 'react-activity/dist/Spinner.css'
import Image from 'next/image'

import logo from '../../../public/logo.svg';
import ImagenFaceboock from "../../../public/SocialMedia/Facebook.svg";
import ImagenInstagram from "../../../public/SocialMedia/Instagram.svg";
import ImagenYoutube from "../../../public/SocialMedia/Youtube.svg"
import editar from "../../../public/editar.svg"
import ImagenTikTok from "../../../public/SocialMedia/TikTok.svg"
import ImgTwitter from "../../../public/SocialMedia/Twitter.svg"
import ImgLinkedin from "../../../public/SocialMedia/messenger.svg"



import { useEffect, useState } from 'react'
import Link from 'next/link';

const TableDatos = (props) => {

    const {reloadClients} = props
    const [isLoading, setIsLoading] = useState(true)

    const [showDots, setShowDots] = useState(true);
    const [total, setTotal] = useState(0);
    const [data, setData] = useState([]);

    useEffect(() => {
        fetch('https://slogan.com.bo/roadie/clients/all')
            .then(response => response.json())
            .then(data => {
                if (data.status) {
                    console.log(data.data);
                    setData(data.data)

                } else {
                    console.error(data.error)
                }
                setIsLoading(false)
            })

            console.warn('reloaddd' + reloadClients);

    }, [reloadClients])

    return (
        isLoading ?
            <>
                <div className='flex justify-center items-center' style={{ width: '100%', height: 50 }}>
                    <Spinner color="#582BE7" size={17} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
                </div>

            </>
            :
            <>
                <div className='rounded-[22.35px] pt-6 h-full w-full overflow-auto'>
                    <TablaProductos data={data} />
                </div>
            </>
    )
}


const TablaProductos = (props) => {
    const { data } = props;
    console.log("assssssssssssssssssss");
    console.log(data);
    console.log(props);
    const map1 = data.map(row => console.log(row));

    const splitData = (obj) => {
        data.forEach(element => {
            let date = element.created.split("T");
            element.created = date[0];
        });
        data = obj;
    };
    splitData(data);

    return (

        <div className="grid grid-cols-12">
            <table className="col-span-12 md:col-span-12 lg:col-span-12">
                <thead>
                    <tr className='text-[14px] md:text-[17px] lg:text-[17px] font-semibold text-[#000000] border-b-2 border-[#D9D9D9]'>
                        <th>Client</th>
                        <th>Social Network</th>
                        <th>Has plan </th>
                        <th> Client since</th>
                    </tr>
                </thead>

                <tbody>

                    {data.map(row =>

                        <tr key={row.id} >

                            <td className=''>
                                <div className='flex flex-row'>
                                    {row.img_url !== null ?
                                        <Image 
                                        className='rounded-full bg-[#F3F3F3]  p-2'
                                        src={row.img_url}
                                        alt="facebook"
                                        height={30}
                                        width={30}>
                  
                                        </Image> 
                                        : <></> 
                                    }
                            
                                
                            
                                    <div className='text-[12px] ml-[10px]'>

                                        {row.name}

                                    </div>
                                </div>
                            

                            </td>

                            <td className=''>
                                <div className='flex flex-row justify-center'>

                                <div className='w-[40px] h-[40px] bg-[#F3F3F3] rounded-[12px] text-center self-center m-[5px]'>
                                    {row.facebook_url !== null ?
                                        <Image
                                        src={ImagenFaceboock}
                                        alt="facebook"
                                        layout="responsive"
                                        width={40}
                                        height={40}
                                        />
                                        : <></>
                                    }
                                </div>
                                <div className='w-[40px] h-[40px] bg-[#F3F3F3] rounded-[12px] text-center self-center m-[5px]'>
                                    {row.instagram_url !== null ?
                                        <Image
                                        src={ImagenInstagram}
                                        alt="instagram"
                                        layout="responsive"
                                        width={40}
                                        height={40}
                                        />
                                        : <></>
                                    }
                                </div>
                                <div className='w-[40px] h-[40px] bg-[#F3F3F3] rounded-[12px] text-center self-center m-[5px]'>
                                    {row.youtube_url !== null ?
                                        <Image
                                        src={ImagenYoutube}
                                        alt="mailing"
                                        layout="responsive"
                                        width={40}
                                        height={40}
                                        />
                                        : <></>
                                    }
                                </div>
                                <div className='w-[40px] h-[40px] bg-[#F3F3F3] rounded-[12px] text-center self-center m-[5px]'>
                                    {row.tiktok_url !== null ?
                                        <Image
                                        src={ImagenTikTok}
                                        alt="youtube"
                                        layout="responsive"
                                        width={40}
                                        height={40}
                                        />
                                        : <></>
                                    }
                                </div>
                                <div className='w-[40px] h-[40px] bg-[#F3F3F3] rounded-[12px] text-center self-center m-[5px]'>
                                    {row.twitter_url !== null ?
                                        <Image
                                        src={ImgTwitter}
                                        alt="tiktok"
                                        layout="responsive"
                                        width={40}
                                        height={40}
                                        />
                                        : <></>
                                    }
                                </div>
                                <div className='w-[40px] h-[40px] bg-[#F3F3F3] rounded-[12px] text-center self-center m-[5px]'>
                                    {row.linkedin_url !== null ?
                                        <Image
                                        src={ImgLinkedin}
                                        alt="linkedin"
                                        layout="responsive"
                                        width={40}
                                        height={40}
                                        />
                                        : <></>
                                    }
                                </div>

                            </div>

                            </td>
                            <td className='text-center'>

                                {row.plans !== null && row.plans.length  > 0  ?

                                <div>
                                    <div className='text-[#643DCE] font-semibold text-[20px] text-center'>YES</div>
                                    <div className='text-[#000000] text-[11px] text-center'>{row.plans[0].name}</div>
                                </div>
                                : <></>
                                }
                            </td>


                            <td className='text-center'>
                                {row.client_since}
                            </td>

                            <td className='text-center'>
                                <Link
                                    href={`/clientDashboard/${encodeURIComponent(row.id)}`}
                                >
                                    <Image
                                        src={editar}
                                        layout="fixed"
                                        width={'100%'}
                                        height={27}
                                        alt='editar' />
                                </Link>
                            </td>

                        </tr>
                    )}


                </tbody>
            </table>
        </div>
    );
};





export default TableDatos;